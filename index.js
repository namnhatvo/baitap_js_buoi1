// bài 1 tính tiền lương nhân viên
const VND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });

function luongNhanVien() {
    var luong1Ngay = document.getElementById("luong_1_ngay").value * 1;
    var soNgayLam = document.getElementById("so_ngay_lam").value * 1;
    var result = luong1Ngay * soNgayLam;

    document.getElementById("result").innerText = `Tổng tiền : ${VND.format(result)} `;
}

// bài 2

function giaTriTB() {
    var a = document.getElementById("so_thu_1").value * 1.0;
    var b = document.getElementById("so_thu_2").value * 1.0;
    var c = document.getElementById("so_thu_3").value * 1.0;
    var d = document.getElementById("so_thu_4").value * 1.0;
    var e = document.getElementById("so_thu_5").value * 1.0;

    var result = (a + b + c + d + e) / 5;
    document.getElementById("results").innerText = `Giá trị trung bình : ${(result)} `;
}

// bài 3

function doiTien() {
    var a = document.getElementById("USD").value * 1;
    var result = a * 23500; 
    document.getElementById("change").innerText = `Tổng tiền : ${VND.format(result)} `;
}

//bài 4
function chuViHCN() {
    var chDai = document.getElementById("chieu_dai").value * 1.0;
    var chRong = document.getElementById("chieu_rong").value * 1.0;
    var chuVi = (chDai + chRong) * 2;
    
    document.getElementById("excute").innerText = `Chu vi HCN : ${(chuVi)} `;
   
}

function dienTichHCN() {
    var chDai = document.getElementById("chieu_dai").value * 1.0;
    var chRong = document.getElementById("chieu_rong").value * 1.0;
    var dienTich = chDai * chRong;
    document.getElementById("excute").innerText = `Diện tích HCN : ${(dienTich)} `;
}

// bài 5

function tong2KySo() {
    var chuSo = document.getElementById("so").value;
    var so_hang_dv = chuSo % 10;
    var so_hang_chuc = chuSo / 10;
    var tong =  parseInt( so_hang_dv + so_hang_chuc);
    document.getElementById("total").innerText = `Tổng 2 ký số : ${(tong)} `;

}